import {Layout, Typography} from 'antd';
import * as React from 'react';
import './../App.css';
import MultiInputField from "./Helpers/MultiInputField/MultiInputField";
import PdfUpload from "./Helpers/PdfUpload/PdfUpload";

const {Title} = Typography;

interface ICutProps {
    stage1Title: string
    addFieldName: string

    uploadHandler(options: any, inputValues: string[]): void,
}

interface IAppState {
    inputFinished: boolean,
    inputValues: string[] | null
}

class Cut extends React.Component<ICutProps, IAppState> {

    constructor(props: any, state: any) {
        super(props, state);
        this.state = {
            inputFinished: false,
            inputValues: null
        };

        this.onMultiInputFieldFinishCallback = this.onMultiInputFieldFinishCallback.bind(this);
        this.onUploadCallback = this.onUploadCallback.bind(this);
    }

    public render() {
        return (
            <Layout id={"cut-layout"}>
                <Layout.Content>
                    <Title className={"stage-title"} disabled={this.state.inputFinished} level={2}>Schritt 1: {this.props.stage1Title}</Title>
                    <MultiInputField disabled={this.state.inputFinished} addFieldName={this.props.addFieldName} inputFinishCallback={this.onMultiInputFieldFinishCallback}/>
                    <Title className={"stage-title"} disabled={!this.state.inputFinished} level={2}>Schritt 2: Zu schneidendes PDF Hochladen</Title>
                    <PdfUpload uploadCallback={this.onUploadCallback} disabled={!this.state.inputFinished}/>
                </Layout.Content>
            </Layout>
        );
    }

    private onMultiInputFieldFinishCallback = (texts: string[]) => {
        this.setState({inputFinished: true, inputValues: texts});
    };

    private onUploadCallback(options: any) {
        this.props.uploadHandler(options, this.state.inputValues ? this.state.inputValues : []);
    }
}

export default Cut;
