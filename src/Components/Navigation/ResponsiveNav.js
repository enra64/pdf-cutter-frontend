import React, { Component } from 'react';
import PropTypes from 'prop-types';
import throttle from 'lodash.throttle';

// use babel-import-plugin as specified in Ant Design Docs!
// https://ant.design/docs/react/getting-started#Import-on-Demand
import Popover from 'antd/lib/popover';
import Icon from 'antd/lib/icon';
import 'antd/lib/popover/style/css';
import 'antd/lib/icon/style/css';

import './styles.css';

class ResponsiveNav extends Component {
    state = {
        menuVisible: false,
        viewportWidth: 0,
    };

    componentDidMount() {
        this.saveViewportDimensions();
        window.addEventListener('resize', this.saveViewportDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.saveViewportDimensions);
    }

    handleMenuVisibility = (menuVisible) => {
        this.setState({ menuVisible });
    };

    saveViewportDimensions = throttle(() => {
        this.setState({
            viewportWidth: window.innerWidth,
        })
    }, this.props.applyViewportChange);

    render() {
        const MenuMarkup = this.props.menuMarkup;

        if (this.state.viewportWidth > this.props.mobileBreakPoint) {
            return <MenuMarkup activeLinkKey={this.props.activeLinkKey} />;
        }

        return (
            <Popover
                content={<MenuMarkup
                    onLinkClick={() => this.handleMenuVisibility(false)}
                    activeLinkKey={this.props.activeLinkKey}
                    mobileVersion
                    // className='to-override-mobile-menu-class'
                />
                }
                trigger='click'
                placement={this.props.placement}
                visible={this.state.menuVisible}
                onVisibleChange={this.handleMenuVisibility}
            >
                <Icon
                    className='iconHamburger'
                    type='menu'
                />
            </Popover>
        );
    }
}

ResponsiveNav.propTypes = {
    activeLinkKey: PropTypes.string,
    applyViewportChange: PropTypes.number,
    menuMarkup: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.object,
    ]),
    mobileBreakPoint: PropTypes.number,
    placement: PropTypes.string,
};

ResponsiveNav.defaultProps = {
    applyViewportChange: 250,
    mobileBreakPoint: 575,
    placement: 'bottom',
};

export default ResponsiveNav