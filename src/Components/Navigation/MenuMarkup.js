import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

// use babel-import-plugin as specified in Ant Design Docs!
// https://ant.design/docs/react/getting-started#Import-on-Demand
import Menu from 'antd/lib/menu';
import 'antd/lib/menu/style/css';

const MenuMarkup = ({ activeLinkKey, className, mobileVersion, onLinkClick,  }) => (
    <Menu
        theme={mobileVersion ? 'light' : 'dark'}
        mode={mobileVersion ? 'vertical' : 'horizontal'}
        selectedKeys={[`${activeLinkKey}`]}
        className={className}
    >
        <Menu.Item key='/'>
            <Link onClick={onLinkClick} to='/'>Abrechnung nach Namen trennen</Link>
        </Menu.Item>
        <Menu.Item key='/about'>
            <Link onClick={onLinkClick} to='/FreeCut'>Beliebige Seiten ausschneiden</Link>
        </Menu.Item>
    </Menu>
);

MenuMarkup.propTypes = {
    activeLinkKey: PropTypes.string.isRequired,
    className: PropTypes.string,
    mobileVersion: PropTypes.bool,
    onLinkClick: PropTypes.func,
};

MenuMarkup.defaultProps = {
    className: 'mobile-navigation',
    mobileVersion: false,
};

export default MenuMarkup;
