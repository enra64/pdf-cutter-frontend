import {Button, Form, Input} from "antd";
import * as React from 'react';

interface ISingleInputFieldProps {
    removeButtonCallback: any
    text: string
    id: string
    hasRemoveButton: boolean
    disabled: boolean

    onTextChanged(myId: string, newValue: string): void
}

export default class SingleInputField extends React.PureComponent<ISingleInputFieldProps> {
    constructor(props: ISingleInputFieldProps) {
        super(props);
        this.updateText = this.updateText.bind(this);
        this.onRemoveButtonClick = this.onRemoveButtonClick.bind(this);
    }

    public render() {
        const {text, disabled, hasRemoveButton} = this.props;
        return (
            <Form layout={"inline"}>
                <Form.Item>
                    <Input disabled={disabled} onChange={this.updateText} value={text}/>
                </Form.Item>
                <Form.Item>
                    {
                        hasRemoveButton && <Button htmlType={"button"} disabled={disabled}
                                                   onClick={this.onRemoveButtonClick}>{"Zeile mit " + (text === "" ? "" : "\"" + text + "\"") + " entfernen"}</Button>
                    }
                </Form.Item>
            </Form>
        );
    }

    private onRemoveButtonClick(): void {
        this.props.removeButtonCallback(this.props.id);
    }

    private updateText(e: any) {
        this.props.onTextChanged(this.props.id, e.target.value);
    }
}

