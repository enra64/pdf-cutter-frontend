import {Button, Layout} from "antd";
import * as React from 'react';
import {v4 as uuidV4} from 'uuid';
import SingleInputField from "./SingleInputField";

interface IMultiInputFieldProps {
    addFieldName: string
    disabled: boolean

    inputFinishCallback(texts: string[]): void
}

interface IMultiInputFieldState {
    inputValues: object
    inputFieldIds: string[]
}

const defaultState = {
    inputFieldIds: [],
    inputValues: {},
};

export default class MultiInputField extends React.Component<IMultiInputFieldProps, IMultiInputFieldState> {
    private stateStorageKey: string;

    constructor(props: IMultiInputFieldProps) {
        super(props);
        this.onAddField = this.onAddField.bind(this);
        this.onRemoveField = this.onRemoveField.bind(this);
        this.onFinishButton = this.onFinishButton.bind(this);
        this.textUpdated = this.textUpdated.bind(this);

        this.stateStorageKey = "state-" + props.addFieldName;

        this.state = this.getDefaultState();
    }

    public componentDidMount(): void {
        if (this.state === defaultState) {
            this.onAddField();
        }
    }

    public render() {
        const inputFields = this.state.inputFieldIds.map((fieldId: string, index: number) =>
                <SingleInputField
                    key={fieldId}
                    id={fieldId}
                    disabled={this.props.disabled}
                    hasRemoveButton={index !== 0}
                    removeButtonCallback={this.onRemoveField}
                    onTextChanged={this.textUpdated}
                    text={this.state.inputValues[fieldId]}/>
        );

        return (
            <Layout aria-disabled={this.props.disabled} id={"multi-input-field"}>
                <Layout>
                    {inputFields}
                    <Button id={"add-button"} disabled={this.props.disabled} htmlType={"button"}
                            onClick={this.onAddField}>{this.props.addFieldName}</Button>
                </Layout>
                <span>
                    <Button id={"finished-button"} type={"primary"} disabled={this.props.disabled} htmlType={"button"}
                            onClick={this.onFinishButton}>Fertig!</Button>
                </span>
            </Layout>
        );
    }

    private onFinishButton(): void {
        this.saveInputState();
        this.props.inputFinishCallback(
            Object.keys(this.state.inputValues)
                .map((key: string) => this.state.inputValues[key])
                .filter((text: string) => !(text === "")
                )
        );
    };

    private textUpdated(updatedFieldId: string, updatedValue: string): void {
        const inputValues = this.state.inputValues;
        inputValues[updatedFieldId] = updatedValue;
        this.setState({
            ...this.state,
            inputValues
        })
    }

    private onAddField = (): void => {
        const newFieldId = uuidV4();

        const newFieldList = [...this.state.inputFieldIds, newFieldId];
        const newFieldValues = {...this.state.inputValues};
        newFieldValues[newFieldId] = "";
        this.setState({
            inputFieldIds: newFieldList,
            inputValues: newFieldValues
        });
    };

    private onRemoveField = (removalId: string): void => {
        const filteredInputFields = this.state.inputFieldIds.filter((id: string) => id !== removalId);
        this.setState({
            inputFieldIds: filteredInputFields
        });
    };

    private getDefaultState(): IMultiInputFieldState {
        const localStorageState = localStorage.getItem(this.stateStorageKey);
        if (localStorageState) {
            const oldState = JSON.parse(localStorageState);

            if (oldState) {
                return oldState;
            }
        }

        return defaultState;
    }

    private saveInputState(): void {
        localStorage.setItem(this.stateStorageKey, JSON.stringify(this.state));
    }
}

