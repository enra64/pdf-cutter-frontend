import {Button, Layout, Upload} from 'antd';
import * as React from 'react';

interface IPdfUploadProps {
    disabled: boolean
    uploadCallback(options: any): void
}

export default class PdfUpload extends React.Component<IPdfUploadProps> {
    public render() {
        const {disabled} = this.props;
        return (
            <Layout id={"pdf-upload"}>
                <Upload.Dragger disabled={disabled} customRequest={this.props.uploadCallback} accept=".pdf" name={"file"}>
                    <Button disabled={disabled}  htmlType={"button"}>
                        Hier klicken oder Datei herziehen, um die PDF-Datei hochzuladen.
                    </Button>
                </Upload.Dragger>
            </Layout>
        );
    }

}

