import axios from "axios";
import {saveAs} from "file-saver";


const cutSpecificationEndpoint = process.env.CUT_SPEC_ENDPOINT ? process.env.CUT_SPEC_ENDPOINT : "http://localhost:8080/generateCutSpecification/bruttoNettoThingy/";
const cuttingEndpoint = process.env.CUT_ENDPOINT ? process.env.CUT_ENDPOINT : "http://localhost:8080/cutpdf";

const getCutSpecificationRequestData = (file: string | Blob, names: string[]): { endpoint: string, data: FormData, config: object } => {
    const data = new FormData();
    data.append('pdfFile', file);
    const config = {
        headers: {
            "content-type": 'multipart/form-data; boundary=----WebKitFormBoundaryqTqJIxvkWFYqvP5s'
        },
        params: {
            "additionalInformation": names.join(",")
        }
    };

    return {endpoint: cutSpecificationEndpoint, data, config}
};

const getFileCuttingRequestData = (file: string | Blob, cutSpecification: string[], pageNames: string[]): { endpoint: string; data: FormData; config: object } => {
    const data = new FormData();
    data.append('pdfFile', file);
    data.append('pages', cutSpecification.join("|"));
    data.append('resultNames', pageNames.join("|"));
    const config = {
        headers: {
            "content-type": 'multipart/form-data; boundary=----WebKitFormBoundaryqTqJIxvkWFYqvP5s'
        },
        responseType: 'blob'
    };

    return {endpoint: cuttingEndpoint, data, config}
};

export const uploadHandlerWithCutSpecGeneration = (options: any, names: string[]) => {
    const cutSpecRequest = getCutSpecificationRequestData(options.file, names);
    axios.post(cutSpecRequest.endpoint, cutSpecRequest.data, cutSpecRequest.config).then((cutSpecResponse: any) => {
        const cutRequest = getFileCuttingRequestData(options.file, cutSpecResponse.data.cutSpecification, cutSpecResponse.data.pageNames);
        axios.post(cutRequest.endpoint, cutRequest.data, cutRequest.config).then((cutResponse: any) => {
                options.onSuccess(cutResponse.data, options.file);
                saveAs(cutResponse.data, "CutFiles.zip");
            }, (err: Error) => {
                options.onError(String(err));
            }
        );
    }).catch((err: Error) => {
        options.onError(String(err));
    })
};

export const uploadHandlerFreeCut = (options: any, cutSpecifications: string[], pageNames: string[] | null = null) => {
    if (pageNames == null) {
        pageNames = cutSpecifications.map((cutSpecification: string) => cutSpecification);
    }

    const cutRequest = getFileCuttingRequestData(options.file, cutSpecifications, pageNames);
    axios.post(cutRequest.endpoint, cutRequest.data, cutRequest.config).then((cutResponse: any) => {
            options.onSuccess(cutResponse.data, options.file);
            saveAs(cutResponse.data, "CutFiles.zip");
        }, (err: Error) => {
            options.onError(String(err));
        }
    );
};

