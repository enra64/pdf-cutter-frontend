import {createBrowserHistory} from "history";
import Cut from "./Components/Cut";
import {uploadHandlerFreeCut, uploadHandlerWithCutSpecGeneration} from "./requestUtils";

import {Layout} from "antd";
import * as React from "react";
import {Route, Router} from "react-router-dom";
import Header from "./Components/Header";

const FreeCut = () => {
    return <Cut addFieldName={"Ich brauche noch ein Schnittmusterfeld!"} stage1Title={"Schnittmuster eingeben"}
                uploadHandler={uploadHandlerFreeCut}/>
};

const NameCut = () => {
    return <Cut stage1Title={"Namen eingeben"} uploadHandler={uploadHandlerWithCutSpecGeneration}
                addFieldName={"Ich brauche noch ein Namensfeld!"}/>
};


const customHistory = createBrowserHistory();

class App extends React.Component {
    public render() {
        return (
            <Router history={customHistory}>
                <Layout style={{height: "100vh"}}>
                    <Layout>
                        <Route path={"/"} exact={false} component={Header}/>
                        <Route path={"/"} exact={true} component={NameCut}/>
                        <Route path={"/FreeCut"} component={FreeCut}/>
                    </Layout>
                </Layout>
            </Router>
        );
    }
}

export default App;
